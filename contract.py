#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, ModelWorkflow, fields
from trytond.pyson import Eval, Not, In, Bool, Or
from trytond.transaction import Transaction
from trytond.pool import Pool


class Contract(ModelWorkflow, ModelSQL, ModelView):
    _name = 'contract.contract'

    account_rule = fields.Many2One('account.account.rule', 'Account Rule',
            states={
                'invisible': Or(Not(Bool(Eval('has_invoice_automation'))),
                        Not(Bool(Eval('automation')))),
                'readonly': In(Eval('state'), ['cancel', 'terminated']),
                }, depends=['has_invoice_automation', 'automation', 'state'])

    def create_invoice_action(self, action):
        action_obj = Pool().get('contract.action')

        context = Transaction().context
        accounting_dates = self._get_accounting_dates(action)
        context['effective_date'] = accounting_dates.get('accounting_date')
        with Transaction().set_context(**context):
            action = action_obj.browse(action.id)
            res = super(Contract, self).create_invoice_action(action)
        return res

    # Taken from account_batch_invoice_timeline
    # TODO: Move both to account_timeline
    def get_account_id_for_date(self, account_id=None, date=None):
        '''
        Get the account_id for a given date.

        :param account_id: the id of the account
        :param date: the date of the account
        :return: the id of the account
        '''
        pool = Pool()
        account_obj = pool.get('account.account')
        contract_obj = pool.get('contract.contract')
        date_obj = pool.get('ir.date')
        today = date_obj.today()
        if account_id is None:
            return -1

        if date is None:
            date = today

        # TODO: Workaround for #1100: take the method for now from contract
        fiscalyear_id = contract_obj.get_fiscalyear_id(date)
        all_account_ids = [account_id]
        all_account_ids.extend(account_obj._get_all_predecessors(account_id))
        all_account_ids.extend(account_obj._get_all_successors(account_id))
        new_account_ids = account_obj.search([
                        ('id', 'in', all_account_ids),
                        ('fiscalyear', '=', fiscalyear_id),
                ])
        if not new_account_ids:
            return -1

        new_account_id = new_account_ids[0]
        return new_account_id

Contract()


class ContractLine(ModelSQL, ModelView):
    _name = 'contract.line'

    def __init__(self):
        super(ContractLine, self).__init__()
        self._error_messages.update({
                'not_apply_contract_rule': 'Can not apply account rule '
                    'of contract "%s" (%s)!',
                })

    def get_invoice_line_account(self, line):
        '''
        Overwrites contract.line.get_invoice_line_account() and replaces
        the detection of revenue or expense accounts by product rules.

        Return the account value of an invoice line for a contract line

        :param line: the BrowseRecord of the contract line
        :return: the id of the invoice line account
        '''
        account_rule_obj = Pool().get('account.account.rule')
        date_obj = Pool().get('ir.date')

        account_id = None
        account_type = 'revenue'
        if line.contract.invoice_type in ('in_invoice', 'in_credit_note'):
            account_type = 'expense'

        if line.contract.account_rule:
            # When there is an account rule given for the contract,
            # try to apply the rule. If the account rule will not apply,
            # raise an error and do not use any default rules.
            rule = line.contract.account_rule
            rule_values = {}
            rule_values['taxes'] = [('set', [x.id for x in line.taxes])]
            account_pattern = account_rule_obj._get_account_rule_pattern(
                line.contract.party_invoice, rule_values)
            account_id = account_rule_obj.apply(rule, account_pattern)
            if not account_id:
                self.raise_user_error('not_apply_contract_rule',
                    error_args=(line.contract.name, line.contract.code))
        else:
            date = Transaction().context.get('effective_date',
                date_obj.today())
            product = None
            if line.product:
                product = line.product
            account_id = account_rule_obj.get_account(
                account_type, line.contract.party_invoice, date, product,
                line.taxes)
        return account_id

ContractLine()

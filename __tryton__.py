# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Contract Automation Invoice Timeline',
    'name_de_DE': 'Vertrag Automatik Fakturierung Gültigkeitsdauer',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Provides timeline features for 'Contract Automation Invoice'
    - Adds a possibility to define a account rule to be used on invoice creation
''',
    'description_de_DE': '''
    - Stellt die Merkmale der Gültigkeitsdauer-Module für das Modul
    'Vertrag Automatik Fakturierung' zur Verfügung.
    - Ermöglicht die Angabe einer Kontenregel pro Vertrag
''',
    'depends': [
        'account_timeline_product_rule',
        'contract_automation_invoice'
    ],
    'xml': [
        'contract.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
